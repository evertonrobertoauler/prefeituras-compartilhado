"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.COLECAO_ACESSOS = 'acessos';
exports.COLECAO_AVISOS = 'avisos';
exports.COLECAO_CALENDARIO = 'calendario';
exports.COLECAO_CATEGORIAS = 'categorias';
var DenunciaSituacao;
(function (DenunciaSituacao) {
    DenunciaSituacao["Lida"] = "Lida, em fila para execu\u00E7\u00E3o";
    DenunciaSituacao["Concluido"] = "Conclu\u00EDdo, processo arquivado";
    DenunciaSituacao["AguardandoRetornoUsuario"] = "Aguardando retorno usu\u00E1rio";
    DenunciaSituacao["Enviada"] = "Enviada, aguardando analise";
    DenunciaSituacao["Enviando"] = "Enviando...";
    DenunciaSituacao["Cancelada"] = "Exclu\u00EDda / Cancelada";
})(DenunciaSituacao = exports.DenunciaSituacao || (exports.DenunciaSituacao = {}));
exports.TIPO_MENSAGEM_SITUACAO = 'Situação foi atualizada';
exports.TIPO_MENSAGEM_TRANSFERENCIA = 'Transferência';
exports.COLECAO_DENUNCIAS = 'denuncias';
exports.COLECAO_INSTITUICOES = 'instituicoes';
exports.COLECAO_INTERACOES = 'interacoes';
exports.COLECAO_NOTICIAS = 'noticias';
exports.COLECAO_CONFIGURACOES_NOTIFICACOES = 'configuracoesNotificacoes';
exports.COLECAO_PERMISSOES = 'permissoes';
exports.TIPOS = {
    avisos: 'Avisos',
    calendario: 'Calendário',
    denuncias: 'Denúncias',
    noticias: 'Notícias',
    pesquisas: 'Pesquisas',
    servicos: 'Serviços',
    sugestoes: 'Sugestões',
    telefonesInformacoes: 'Telefones/Informações',
    interacoes: 'Interações',
    instituicoes: 'Instituições',
    formularios: 'Formulários'
};
exports.COLECAO_PESQUISAS = 'pesquisas';
var ServicoSituacao;
(function (ServicoSituacao) {
    ServicoSituacao["Lido"] = "Lido, em fila para execu\u00E7\u00E3o";
    ServicoSituacao["Concluido"] = "Conclu\u00EDdo, processo arquivado";
    ServicoSituacao["AguardandoRetornoUsuario"] = "Aguardando retorno usu\u00E1rio";
    ServicoSituacao["Enviado"] = "Enviado, aguardando analise";
    ServicoSituacao["Enviando"] = "Enviando...";
    ServicoSituacao["Cancelado"] = "Exclu\u00EDdo / Cancelado";
})(ServicoSituacao = exports.ServicoSituacao || (exports.ServicoSituacao = {}));
exports.COLECAO_SERVICOS = 'servicos';
exports.COLECAO_SUGESTOES = 'sugestoes';
exports.COLECAO_TELEFONES = 'telefonesInformacoes';
exports.AUTH_PROVIDERS = {
    'facebook.com': 'facebook',
    'google.com': 'google',
    phone: 'telefone'
};
exports.COLECAO_USUARIOS = 'usuarios';
exports.COLECAO_FORMULARIOS = 'formularios';
exports.COLECAO_VERSOES_APLICATIVO = 'versoesAplicativo';
