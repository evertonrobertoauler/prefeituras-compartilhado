"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function obterInformacoesCompartilhamento(colecao, id, obj) {
    var titulo, imagem, descricao;
    switch (colecao) {
        case 'telefonesInformacoes':
            titulo = obj.nome;
            break;
        case 'avisos':
            var aviso = obj;
            titulo = aviso.titulo;
            imagem = aviso.imagem.url;
            descricao = toPlaintext(aviso.descricao);
            break;
        case 'pesquisas':
            var pesquisa = obj;
            titulo = pesquisa.titulo;
            imagem = pesquisa.imagem.url;
            descricao = pesquisa.descricao;
            break;
        case 'noticias':
            var noticia = obj;
            titulo = noticia.titulo;
            imagem = noticia.imagem.url;
            for (var _i = 0, _a = noticia.conteudo; _i < _a.length; _i++) {
                var conteudo = _a[_i];
                if (conteudo.tipo === 'texto') {
                    descricao = toPlaintext(conteudo.texto);
                    break;
                }
            }
            break;
        default:
            return null;
    }
    return { titulo: titulo, imagem: imagem, descricao: descricao };
}
exports.obterInformacoesCompartilhamento = obterInformacoesCompartilhamento;
function toPlaintext(deltaJson) {
    if (deltaJson) {
        if (typeof deltaJson === 'object' && 'texto' in deltaJson) {
            return deltaJson.texto;
        }
        else if (typeof deltaJson === 'string') {
            var delta = (deltaJson && JSON.parse(deltaJson).ops) || [];
            return delta.reduce(function (text, op) {
                if (!op.insert) {
                    return text;
                }
                if (typeof op.insert !== 'string') {
                    return text + ' ';
                }
                return text + op.insert;
            }, '');
        }
    }
}
exports.toPlaintext = toPlaintext;
