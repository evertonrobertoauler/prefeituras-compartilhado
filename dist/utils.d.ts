import { TIPO, TelefoneItem, Aviso, Pesquisa, Noticia } from './colecoes';
export interface QuillDelta {
    ops: any[];
}
export interface TrixDelta {
    texto: string;
    html: string;
}
export declare function obterInformacoesCompartilhamento(colecao: TIPO, id: string, obj: TelefoneItem | Aviso | Pesquisa | Noticia): {
    titulo: any;
    imagem: any;
    descricao: any;
};
export declare function toPlaintext(deltaJson: string | TrixDelta): any;
