import { Timestamp, FieldValue } from '@firebase/firestore-types';
import { Observable } from 'rxjs';
export interface AcessoUsuario {
    usuario: string;
    data?: Timestamp;
}
export declare const COLECAO_ACESSOS = "acessos";
export interface Imagem {
    id?: string;
    url?: string;
    caminho?: string;
    arquivo?: File;
    base64?: string;
    minificada?: boolean;
}
export interface ImagemIonicStorage {
    caminho: string;
    colecao: string;
    colecaoId: string;
    arquivo: string;
    campoUrl: string;
}
export declare const COLECAO_AVISOS = "avisos";
export interface Aviso {
    id?: string;
    usuario: string;
    cadastro?: Timestamp;
    categoria: string;
    imagem: Imagem;
    titulo: string;
    descricao: string;
    notificar: boolean;
    notificado: boolean;
}
export interface Evento {
    id?: string;
    instituicao: string;
    objInstituicao?: Instituicao;
    titulo: string;
    dataInicio: string;
    dataFiltro?: string;
    dataFim?: string;
    descricao?: string;
    usuario: string;
}
export declare const COLECAO_CALENDARIO = "calendario";
export declare type TIPO = 'denuncias' | 'servicos' | 'sugestoes' | 'avisos' | 'noticias' | 'pesquisas' | 'telefonesInformacoes' | 'calendario' | 'usuarios' | 'acessos' | 'interacoes' | 'permissoes' | 'instituicoes' | 'formularios';
export interface Categoria {
    id?: string;
    tipo: TIPO;
    descricao: string;
    ordenacao: string;
    interna: boolean;
}
export interface CacheObservables {
    ids: {
        [id: string]: Observable<Categoria>;
    };
    tipos: {
        [tipo: string]: Observable<Categoria[]>;
    };
}
export declare const COLECAO_CATEGORIAS = "categorias";
export declare enum DenunciaSituacao {
    Lida = "Lida, em fila para execu\u00E7\u00E3o",
    Concluido = "Conclu\u00EDdo, processo arquivado",
    AguardandoRetornoUsuario = "Aguardando retorno usu\u00E1rio",
    Enviada = "Enviada, aguardando analise",
    Enviando = "Enviando...",
    Cancelada = "Exclu\u00EDda / Cancelada"
}
export interface Denuncia {
    id?: string;
    protocolo?: string;
    cadastro?: Timestamp;
    usuario: string;
    usuario$?: Observable<Usuario>;
    categoria: string;
    categoria$?: Observable<Categoria>;
    descricao: string;
    localizacao: string;
    coordenada: string;
    situacao: DenunciaSituacao;
    fotos: Imagem[];
    pessoa: {
        anonima?: boolean;
        terceiro: boolean;
        nome: string;
        cpf: string;
        telefone: string;
    };
    transferencia?: Transferencia;
}
export interface DenunciasPermissao extends Categoria {
    denuncias$: Observable<Denuncia[]>;
}
export declare const TIPO_MENSAGEM_SITUACAO = "Situa\u00E7\u00E3o foi atualizada";
export declare const TIPO_MENSAGEM_TRANSFERENCIA = "Transfer\u00EAncia";
export interface Transferencia {
    usuario: string;
    categoria: string;
    mensagem: string;
}
export interface Mensagem {
    id: string;
    usuario: string;
    usuario$?: Observable<Usuario>;
    mensagem: string;
    data?: Timestamp;
    notificou?: boolean;
}
export declare const COLECAO_DENUNCIAS = "denuncias";
export interface Instituicao {
    id?: string;
    nome: string;
    ordenacao: string;
    imagem: Imagem;
}
export declare const COLECAO_INSTITUICOES = "instituicoes";
export interface Interacao {
    id?: string;
    protocolo?: string;
    cadastro?: Timestamp;
    usuario: string;
    usuario$?: Observable<Usuario>;
    categoria: string;
    categoria$?: Observable<Categoria>;
    descricao: string;
    situacao: string;
    fotos: Imagem[];
    pessoa: {
        nome: string;
    };
}
export interface InteracoesPermissao extends Categoria {
    interacoes$: Observable<Interacao[]>;
}
export declare const COLECAO_INTERACOES = "interacoes";
export declare type CONTEUDO = {
    tipo: 'texto';
    texto: string;
} | {
    tipo: 'imagem';
    imagem: Imagem;
};
export interface Noticia {
    id?: string;
    usuario: string;
    cadastro?: Timestamp;
    categoria: string;
    imagem: Imagem;
    titulo: string;
    conteudo: CONTEUDO[];
}
export declare const COLECAO_NOTICIAS = "noticias";
export interface Configuracao {
    id: string;
    usuario: string;
    tipo: TIPO;
    categoria: string;
}
export declare const COLECAO_CONFIGURACOES_NOTIFICACOES = "configuracoesNotificacoes";
export interface ConfiguracaoPermissaoFormularios {
    somenteRegistros: boolean;
}
export declare type ConfiguracaoPermissao = ConfiguracaoPermissaoFormularios;
export interface Permissao {
    id?: string;
    usuario: string;
    usuario$?: Observable<Usuario>;
    tipo: TIPO;
    categoria?: string;
    categoria$?: Observable<Categoria>;
    ordenacao?: string;
    configuracao?: ConfiguracaoPermissao;
}
export declare const COLECAO_PERMISSOES = "permissoes";
export declare const TIPOS: {
    avisos: string;
    calendario: string;
    denuncias: string;
    noticias: string;
    pesquisas: string;
    servicos: string;
    sugestoes: string;
    telefonesInformacoes: string;
    interacoes: string;
    instituicoes: string;
    formularios: string;
};
export interface Opcao {
    valor?: any;
    descricao?: string;
}
export interface ConfigEscalaLinear {
    inicio: {
        valor: number;
        descricao: string;
    };
    fim: {
        valor: number;
        descricao: string;
    };
}
export declare type TIPO_PERGUNTA_SIMPLES = 'marcar-um' | 'marcar-um-ou-mais' | 'texto' | 'escala';
export declare type TIPO_PERGUNTA = TIPO_PERGUNTA_SIMPLES | 'subformulario';
export interface PerguntaMarcaUm {
    tipo: 'marcar-um';
    texto: string;
    obrigatorio: boolean;
    opcoes: Opcao[];
}
export interface PerguntaMarcaUmOuMais {
    tipo: 'marcar-um-ou-mais';
    texto: string;
    obrigatorio: boolean;
    opcoes: Opcao[];
}
export interface PerguntaTexto {
    tipo: 'texto';
    texto: string;
    obrigatorio: boolean;
}
export interface PerguntaEscala {
    tipo: 'escala';
    texto: string;
    obrigatorio: boolean;
    opcoes: ConfigEscalaLinear;
}
export declare type PerguntaSimples = PerguntaMarcaUm | PerguntaMarcaUmOuMais | PerguntaTexto | PerguntaEscala;
export interface PerguntaSubformulario {
    tipo: 'subformulario';
    texto: string;
    obrigatorio: boolean;
    perguntas: PerguntaSimples[];
}
export declare type Pergunta = PerguntaSimples | PerguntaSubformulario;
export interface Pesquisa {
    id?: string;
    usuario: string;
    usuario$: Observable<Usuario>;
    cadastro?: Timestamp;
    categoria: string;
    categoria$: Observable<Categoria>;
    imagem: Imagem;
    titulo: string;
    descricao: string;
    dataInicio: string;
    dataFim: string;
    perguntas: Pergunta[];
    notificar: boolean;
    notificado: boolean;
    participacoes?: number;
    participacoes$: Observable<Participacao>;
    aberta: boolean;
}
export interface Participacao {
    id: string;
    usuario: string;
    cadastro?: Timestamp;
    respostas: {
        [pergunta: number]: any;
    };
}
export declare const COLECAO_PESQUISAS = "pesquisas";
export declare enum ServicoSituacao {
    Lido = "Lido, em fila para execu\u00E7\u00E3o",
    Concluido = "Conclu\u00EDdo, processo arquivado",
    AguardandoRetornoUsuario = "Aguardando retorno usu\u00E1rio",
    Enviado = "Enviado, aguardando analise",
    Enviando = "Enviando...",
    Cancelado = "Exclu\u00EDdo / Cancelado"
}
export interface Servico {
    id?: string;
    protocolo?: string;
    cadastro?: Timestamp;
    usuario: string;
    usuario$?: Observable<Usuario>;
    categoria: string;
    categoria$?: Observable<Categoria>;
    descricao: string;
    localizacao: string;
    coordenada: string;
    situacao: ServicoSituacao;
    fotos: Imagem[];
    pessoa: {
        terceiro: boolean;
        nome: string;
        cpf: string;
        telefone: string;
    };
    transferencia?: Transferencia;
}
export interface ServicosPermissao extends Categoria {
    servicos$: Observable<Servico[]>;
}
export declare const COLECAO_SERVICOS = "servicos";
export interface Sugestao {
    id?: string;
    protocolo?: string;
    cadastro?: Timestamp;
    usuario: string;
    usuario$?: Observable<Usuario>;
    categoria: string;
    categoria$?: Observable<Categoria>;
    descricao: string;
    situacao: string;
    fotos: Imagem[];
    pessoa: {
        nome: string;
    };
}
export interface SugestoesPermissao extends Categoria {
    sugestoes$: Observable<Sugestao[]>;
}
export declare const COLECAO_SUGESTOES = "sugestoes";
export interface TelefoneItem {
    id: string;
    categoria: string;
    categoria$?: Observable<Categoria>;
    nome: string;
    geral?: {
        telefones: {
            numero: string;
        }[];
        endereco: string;
        coordenada: {
            latitude: number;
            longitude: number;
        };
        informacoes: string;
    };
    maisInformacoes?: CONTEUDO[];
}
export declare const COLECAO_TELEFONES = "telefonesInformacoes";
export interface Usuario {
    id?: string;
    nome?: string;
    cargo?: string;
    cpf?: string;
    telefone?: string;
    email?: string;
    foto?: Imagem;
    providers?: {
        [providerId: string]: {
            id: string;
            nome: string;
            foto: string;
            telefone: string;
            email: string;
        };
    };
    idAnonimo?: string;
}
export declare const AUTH_PROVIDERS: {
    'facebook.com': string;
    'google.com': string;
    phone: string;
};
export declare const COLECAO_USUARIOS = "usuarios";
export declare type OPERACAO = 'gravar' | 'atualizar' | 'excluir';
export interface Log {
    operacao: OPERACAO;
    data: FieldValue;
    usuario: string;
    caminho: string;
    valor: any;
}
export declare type FIRE_OPERACAO = 'insert' | 'update' | 'delete';
export interface FireLog<T = any> {
    data: Timestamp;
    operacao: FIRE_OPERACAO;
    caminho: string;
    atual: T;
    anterior: T;
    mudanca: Partial<T>;
}
export interface Formulario {
    id?: string;
    usuario: string;
    usuario$: Observable<Usuario>;
    cadastro?: Timestamp;
    categoria: string;
    categoria$: Observable<Categoria>;
    imagem: Imagem;
    titulo: string;
    descricao: string;
    dataInicio: string;
    dataFim: string;
    perguntas: Pergunta[];
    registros?: number;
    registros$: Observable<RegistroFormulario>;
    aberto: boolean;
}
export interface RegistroFormulario {
    id: string;
    usuario: string;
    usuario$?: Observable<Usuario>;
    cadastro?: Timestamp;
    respostas: {
        [pergunta: number]: any;
    };
}
export declare const COLECAO_FORMULARIOS = "formularios";
export interface VersaoAtual {
    tipo: 'atual';
    web: string;
    android: string;
    ios: string;
}
export interface VersaoInstalada {
    tipo: 'versao';
    versao: string;
    atual: {
        web: boolean;
        android: boolean;
        ios: boolean;
    };
    validade?: Timestamp;
}
export declare type VersaoAplicativo = VersaoAtual | VersaoInstalada;
export declare const COLECAO_VERSOES_APLICATIVO = "versoesAplicativo";
