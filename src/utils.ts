import { TIPO, TelefoneItem, Aviso, Pesquisa, Noticia } from './colecoes';

export interface QuillDelta {
  ops: any[];
}

export interface TrixDelta {
  texto: string;
  html: string;
}

export function obterInformacoesCompartilhamento(
  colecao: TIPO,
  id: string,
  obj: TelefoneItem | Aviso | Pesquisa | Noticia
) {
  let titulo, imagem, descricao;

  switch (colecao as TIPO) {
    case 'telefonesInformacoes':
      titulo = (obj as TelefoneItem).nome;
      break;
    case 'avisos':
      const aviso = obj as Aviso;
      titulo = aviso.titulo;
      imagem = aviso.imagem.url;
      descricao = toPlaintext(aviso.descricao);
      break;
    case 'pesquisas':
      const pesquisa = obj as Pesquisa;
      titulo = pesquisa.titulo;
      imagem = pesquisa.imagem.url;
      descricao = pesquisa.descricao;
      break;
    case 'noticias':
      const noticia = obj as Noticia;
      titulo = noticia.titulo;
      imagem = noticia.imagem.url;

      for (const conteudo of noticia.conteudo) {
        if (conteudo.tipo === 'texto') {
          descricao = toPlaintext(conteudo.texto);
          break;
        }
      }
      break;
    default:
      return null;
  }

  return { titulo, imagem, descricao };
}

export function toPlaintext(deltaJson: string | TrixDelta) {
  if (deltaJson) {
    if (typeof deltaJson === 'object' && 'texto' in deltaJson) {
      return (deltaJson as TrixDelta).texto;
    } else if (typeof deltaJson === 'string') {
      const delta = (deltaJson && (JSON.parse(deltaJson) as QuillDelta).ops) || [];

      return delta.reduce(function(text, op) {
        if (!op.insert) {
          return text;
        }
        if (typeof op.insert !== 'string') {
          return text + ' ';
        }
        return text + op.insert;
      }, '');
    }
  }
}
